---
layout: markdown_page
title: "Online Marketing"
---
Welcome to the Online Marketing Handbook.

## On this page

* [URL Tagging](#urlTagging)

## URL Tagging<a name="urlTagging"></a>

All marketing campaigns that are promoted on external sites and through email should use URL tagging to increase the data cleanliness in Google Analytics. For a primer course, please see the [training video](https://drive.google.com/a/gitlab.com/file/d/0B1_ZzeTfG3XYNWVqOC11NWpKWjA/view?usp=sharing). This explains the why and how of URL tagging.

Our internal URL tagging tool can be accessed on Google Docs under the name [Google Analytics Campaign Tagging Tool](https://docs.google.com/a/gitlab.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit?usp=sharing).
